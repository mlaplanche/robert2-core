using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Serializers.NewtonsoftJson;
using Robert2.Models;

namespace Robert2.Helpers;

public class LoxyaApiAuthenticator : AuthenticatorBase
{
    private readonly string _baseUrl;
    private readonly string _password;
    private readonly string _username;

    public LoxyaApiAuthenticator(string baseUrl, string username, string password) : base("")
    {
        _baseUrl = baseUrl;
        _username = username;
        _password = password;
    }

    protected override async ValueTask<Parameter> GetAuthenticationParameter(string accessToken)
    {
        Token = string.IsNullOrEmpty(Token) ? await GetToken() : Token;

        return new HeaderParameter(KnownHeaders.Authorization, $"Bearer {Token}");
    }

    private async Task<string> GetToken()
    {
        var options = new RestClientOptions(_baseUrl);

        using var client = new RestClient(
            options,
            configureSerialization: config => config.UseNewtonsoftJson()
        );

        var body = new { identifier = _username, password = _password };
        var request = new RestRequest("session").AddJsonBody(body);
        var response = await client.ExecutePostAsync(request);

        if (response.IsSuccessful)
        {
            var tokenObject = JsonConvert.DeserializeObject<TokenResponse>(response.Content!);

            return tokenObject!.Token;
        }

        response.ThrowIfError();

        return string.Empty;
    }
}