using Microsoft.Extensions.Logging;
using RestSharp;
using Robert2.Models;

namespace Robert2.Managers;

public class CompanyManager : GenericManager<Company>
{
    public CompanyManager(Robert2Config apiConf, ILogger<CompanyManager> logger) : base(apiConf, "companies", logger)
    {
    }

    public async Task<Company?> Create(Company company)
    {
        var companyRobert = await Get(company.Name);
        if (companyRobert is not null) return companyRobert;

        return await PostAsync(company);
    }

    public async Task<Company?> Get(string name)
    {
        var request = new RestRequest()
            .AddQueryParameter("searchBy", "legal_name")
            .AddQueryParameter("search", name)
            .AddQueryParameter("paginated", "0");
        var searchResult = await GetListAsync(request);

        return searchResult?.FirstOrDefault();
    }
}