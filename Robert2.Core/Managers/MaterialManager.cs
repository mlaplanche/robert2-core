using Microsoft.Extensions.Logging;
using RestSharp;
using Robert2.Interfaces;
using Robert2.Models;

namespace Robert2.Managers;

public class MaterialManager : GenericManager<Material>, IMaterialManager
{
    public MaterialManager(Robert2Config apiConf, ILogger<MaterialManager> logger) : base(apiConf, "materials", logger)
    {
    }

    public async Task<IList<Material>> GetAllMaterials(string search = "", int limit = 100,
        bool ascending = true, int page = 1, string orderBy = "name", string startDate = "", string endDate = "")
    {
        var request = new RestRequest()
            .AddQueryParameter("paginated", 0)
            .AddQueryParameter("search", search)
            .AddQueryParameter("limit", limit)
            .AddQueryParameter("page", page)
            .AddQueryParameter("ascending", ascending)
            .AddQueryParameter("orderBy", orderBy)
            .AddQueryParameter("dateForQuantities[start]", startDate)
            .AddQueryParameter("dateForQuantities[end]", endDate);


        return await GetListAsync(request) ?? new List<Material>();
    }

    public async Task<Material?> GetMaterial(int id)
    {
        var request = new RestRequest($"/{id}").AddUrlSegment("id", id.ToString());

        return await GetAsync(request);
    }

    public async Task<object?> GetMaterialImage(int idMaterial)
    {
        using var client = GetClient(ApiConf.AppUrl!);

        var request = new RestRequest(Endpoint + "/{idMaterial}/picture")
            .AddUrlSegment("idMaterial", idMaterial.ToString());

        Logger.LogInformation("DOWNLOAD STREAM {BaseAddress}{Endpoint}/{IdMaterial}/picture",
            client.Options.BaseUrl?.ToString() ?? "NO BASE ADDRESS",
            Endpoint,
            idMaterial.ToString());

        var response = await client.DownloadDataAsync(request);

        return response != null ? new MemoryStream(response) : null;
    }
}