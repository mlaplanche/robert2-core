using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;

namespace Robert2.Managers;

public class GenericManager<T> where T : class
{
    protected readonly Robert2Config ApiConf;
    protected readonly string Endpoint;
    protected readonly ILogger Logger;

    public GenericManager(Robert2Config apiConf, string endpoint, ILogger logger)
    {
        ApiConf = apiConf;
        Endpoint = endpoint;
        Logger = logger;
    }

    protected LoxyaApiClient GetClient(string customBaseUrl = "")
    {
        return new LoxyaApiClient(ApiConf, customBaseUrl);
    }

    /// <summary>
    ///     Obtient l'objet via une rest request
    /// </summary>
    /// <param name="request">requête permettant d'obtenir T.</param>
    /// <returns>
    ///     Un objet T, ou null si aucune correspondance de ce type n'existe.
    /// </returns>
    public async Task<T?> GetAsync(RestRequest request)
    {
        if (string.IsNullOrWhiteSpace(request.Resource))
            request.Resource = Endpoint;
        else
            request.Resource = Endpoint + request.Resource;

        using var client = GetClient();
        Logger.LogInformation("GET {BaseAddress}{Endpoint}",
            client.Options.BaseUrl?.ToString() ?? "NO BASE ADDRESS", request.Resource);

        // Exécute la requête GET de manière asynchrone
        var result = await client.ExecuteGetAsync(request);
        var content = result.Content;

        Logger.LogDebug("GET RESULT {BaseAddress}{Endpoint} \n\r {Content}",
            client.Options.BaseUrl?.ToString() ?? "NO BASE ADDRESS", request.Resource, content);

        // Désérialise le contenu de la réponse en objet et le retourne
        if (result.IsSuccessStatusCode) return JsonConvert.DeserializeObject<T>(content!);

        Logger.LogError("GET ERROR {BaseAddress}{Endpoint} \r\n {ErrorMessage} \n\r {Content}",
            client.Options.BaseUrl?.ToString() ?? "NO BASE ADDRESS", request.Resource, result.ErrorMessage, content);

        // Si exception, on throw !
        result.ThrowIfError();

        return default;
    }

    /// <summary>
    ///     Obtient l'objet via une rest request
    /// </summary>
    /// <param name="request">requête permettant d'obtenir T.</param>
    /// <returns>
    ///     Un objet T, ou null si aucune correspondance de ce type n'existe.
    /// </returns>
    public async Task<IList<T>?> GetListAsync(RestRequest request)
    {
        if (string.IsNullOrWhiteSpace(request.Resource))
            request.Resource = Endpoint;
        else
            request.Resource = Endpoint + request.Resource;

        using var client = GetClient();
        Logger.LogInformation("GET {BaseAddress}{Endpoint}",
            client.Options.BaseUrl?.ToString() ?? "NO BASE ADDRESS", request.Resource);

        // Execute la requête GET asynchrone
        var result = await client.ExecuteGetAsync(request);
        var content = result.Content;

        Logger.LogDebug("GET RESULT {BaseAddress}{Endpoint} \n\r {Content}",
            client.Options.BaseUrl?.ToString() ?? "NO BASE ADDRESS", request.Resource, content);

        // Désérialise le contenu de la réponse en objet et le retourne
        if (result.IsSuccessStatusCode) return JsonConvert.DeserializeObject<IList<T>>(content!);

        Logger.LogError("GET ERROR {BaseAddress}{Endpoint} \r\n {ErrorMessage} \n\r {Content}",
            client.Options.BaseUrl?.ToString() ?? "NO BASE ADDRESS", request.Resource, result.ErrorMessage, content);

        // Si exception, on throw !
        result.ThrowIfError();

        return default;
    }

    /// <summary>
    ///     Crée un nouvel objet de T.
    /// </summary>
    /// <param name="obj">L'objet T représentant l'objet à créer.</param>
    /// <returns>Un objet représentant l'objet nouvellement créée.</returns>
    public async Task<T?> PostAsync(T obj)
    {
        // Création du client
        using var client = GetClient();

        // Crée une nouvelle requête REST avec l'URL et ajoute l'objet dans le corps de requête
        var request = new RestRequest(Endpoint).AddJsonBody(obj);

        Logger.LogInformation("POST {BaseAddress}{Endpoint} : {Object}",
            client.Options.BaseUrl?.ToString() ?? "NO BASE ADDRESS",
            request.Resource,
            JsonConvert.SerializeObject(obj));

        // Execute la requête POST de asynchrone
        var result = await client.ExecutePostAsync(request);
        var content = result.Content;

        Logger.LogDebug("POST RESULT {BaseAddress}{Endpoint} \n\r {Content}",
            client.Options.BaseUrl?.ToString() ?? "NO BASE ADDRESS", request.Resource, content);

        // Désérialise le contenu de la réponse en objet et le retourne
        if (result.IsSuccessStatusCode) return JsonConvert.DeserializeObject<T>(content!);

        Logger.LogError("POST ERROR {BaseAddress}{Endpoint} \r\n {ErrorMessage} \n\r {Content}",
            client.Options.BaseUrl?.ToString() ?? "NO BASE ADDRESS",
            request.Resource,
            result.ErrorMessage,
            content);

        // Si exception, on throw !
        result.ThrowIfError();

        return default;
    }

    /// <summary>
    ///     Met à jour un objet existant.
    /// </summary>
    /// <param name="request">Requête avec URL et body de l'objet à mettre à jour</param>
    /// <returns>Un objet T représentant l'objet mise à jour.</returns>
    public async Task<T?> PutAsync(RestRequest request)
    {
        if (string.IsNullOrWhiteSpace(request.Resource))
            request.Resource = Endpoint;
        else
            request.Resource = Endpoint + request.Resource;

        // Création du client
        using var client = GetClient();

        Logger.LogInformation("PUT {BaseAddress}{Endpoint} \n\r  {Object}",
            client.Options.BaseUrl?.ToString() ?? "NO BASE ADDRESS",
            request.Resource,
            request.Parameters.FirstOrDefault(p => p.Type == ParameterType.RequestBody)?.Value ?? "");

        // Exécute la requête PUT de manière asynchrone
        var result = await client.ExecutePutAsync(request);
        var content = result.Content;

        Logger.LogDebug("PUT RESULT {BaseAddress}{Endpoint} \n\r {Content}",
            client.Options.BaseUrl?.ToString() ?? "NO BASE ADDRESS", request.Resource, content);

        // Désérialise le contenu de la réponse en objet et le retourne
        if (result.IsSuccessStatusCode) return JsonConvert.DeserializeObject<T>(content!);

        Logger.LogError("PUT ERROR {BaseAddress}{Endpoint} \r\n {ErrorMessage} \n\r {Content}",
            client.Options.BaseUrl?.ToString() ?? "NO BASE ADDRESS",
            request.Resource,
            result.ErrorMessage,
            content);

        // Si exception, on throw !
        result.ThrowIfError();

        return default;
    }
}