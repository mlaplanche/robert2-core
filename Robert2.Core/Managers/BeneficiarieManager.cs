using Microsoft.Extensions.Logging;
using RestSharp;
using Robert2.Models;

namespace Robert2.Managers;

public class BeneficiarieManager : GenericManager<Beneficiarie>
{
    private readonly CompanyManager _companyManager;

    public BeneficiarieManager(Robert2Config apiConf, ILogger<BeneficiarieManager> logger, CompanyManager companyManager)
        : base(apiConf, "beneficiaries", logger)
    {
        _companyManager = companyManager;
    }

    public async Task<Beneficiarie?> CreateBeneficiarie(Beneficiarie beneficiarie)
    {
        return await PostAsync(beneficiarie);
    }

    public async Task<Beneficiarie?> CreateBeneficiarie(Beneficiarie pBeneficiarie, Company pCompany)
    {
        var company = await _companyManager.Create(pCompany);

        if (company == null)
            throw new ApplicationException("Impossible de créer l'entreprise " + company!.Name);

        pBeneficiarie.CompanyId = company.Id;

        return await PostAsync(pBeneficiarie);
    }

    public async Task<Beneficiarie?> UpdatePerson(Beneficiarie beneficiarie)
    {
        var request = new RestRequest("/{id}").AddUrlSegment("id", beneficiarie.Id.ToString()!)
            .AddBody(beneficiarie);

        return await PutAsync(request);
    }

    public async Task<Beneficiarie?> GetPerson(int idPerson)
    {
        var request = new RestRequest("/{id}").AddUrlSegment("id", idPerson.ToString());

        return await GetAsync(request);
    }

    public async Task<Beneficiarie?> GetPerson(string email)
    {
        var request = new RestRequest()
            .AddQueryParameter("searchBy", "email")
            .AddQueryParameter("search", email)
            .AddQueryParameter("paginated", "0");

        var persons = await GetListAsync(request);


        return persons?.FirstOrDefault(p => string.Equals(p.Email.Trim().ToLower(), email.Trim().ToLower()));
    }

    public string GetRobertPublicUrl(string id)
    {
        return ApiConf.PublicUrl + "beneficiaries/" + id;
    }
}