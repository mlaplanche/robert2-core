using Microsoft.Extensions.Logging;
using RestSharp;
using Robert2.Models;

namespace Robert2.Managers;

public class CategoryManager : GenericManager<Category>
{
    public CategoryManager(Robert2Config apiConf, ILogger<CategoryManager> logger) : base(apiConf, "categories", logger)
    {
    }

    public async Task<IList<Category>> GetAllCategories(string search = "", int limit = 100,
        bool ascending = true, int page = 1, string orderBy = "name")
    {
        var request = new RestRequest()
            .AddQueryParameter("paginated", 0)
            .AddQueryParameter("search", search)
            .AddQueryParameter("limit", limit)
            .AddQueryParameter("page", page)
            .AddQueryParameter("ascending", ascending)
            .AddQueryParameter("orderBy", orderBy);


        return await GetListAsync(request) ?? new List<Category>();
    }
}