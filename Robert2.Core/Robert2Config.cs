﻿namespace Robert2;

public class Robert2Config
{
    public string? ApiUrl { get; set; }
    public string? AppUrl { get; set; }

    public string? PublicUrl { get; set; }
    public string? Identifier { get; set; }
    public string? Password { get; set; }
}