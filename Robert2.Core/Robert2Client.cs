using System.Net;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;

namespace Robert2;

[Obsolete("Use LoxyaApiClient insted", true)]
public class Robert2Client
{
    private readonly RestClient _clientApi;
    private readonly HttpClient _clientApp;
    private readonly Robert2Config _config;
    private readonly ILogger<Robert2Client> _logger;
    private DateTimeOffset _tokenExpirationDate = DateTimeOffset.Now;

    public Robert2Client(Robert2Config config, ILogger<Robert2Client> logger)
    {
        _logger = logger;
        _config = config;
        _clientApi =
            new RestClient(
                new RestClientOptions(_config.ApiUrl!)
                {
                    ThrowOnAnyError = true
                },
                configureSerialization: serializerConfig => serializerConfig.UseNewtonsoftJson()
            );
        _clientApp = new HttpClient
        {
            BaseAddress = new Uri(_config.AppUrl!)
        };

        Authentication().Wait();
    }

    private async Task CheckTokenExpiration()
    {
        _logger.Log(LogLevel.Information, "--- Check Token expiration ---");
        _logger.Log(LogLevel.Information, $"{_tokenExpirationDate.ToString("F")} < {DateTimeOffset.Now.ToString("F")}");
        _logger.Log(LogLevel.Information, $" is expired {_tokenExpirationDate < DateTimeOffset.Now}");
        if (_tokenExpirationDate < DateTimeOffset.Now) await Authentication();
    }

    private async Task Authentication()
    {
        _logger.Log(LogLevel.Information, "--- Robert Authentication ---");
        if (_clientApi.DefaultParameters.GetParameters(ParameterType.HttpHeader)
                .FirstOrDefault(p => p.Name == "Authorization") != null)
        {
            _logger.Log(LogLevel.Debug, "Remove Authorization header");
            _clientApi.DefaultParameters.RemoveParameter("Authorization", ParameterType.HttpHeader);
            _clientApp.DefaultRequestHeaders.Remove("Authorization");
        }

        var body = new { identifier = _config.Identifier, password = _config.Password };
        var request = new RestRequest("session").AddJsonBody(body);
        var jsonResponse = await _clientApi.PostAsync<dynamic>(request);
        var response = JsonConvert.DeserializeAnonymousType(jsonResponse?.ToString(), new { token = "" });

        _logger.Log(LogLevel.Debug, "Add Authorization header with bearer");
        try
        {
            _clientApi.AddDefaultHeader("Authorization", $"Bearer {response!.token}");
            _clientApp.DefaultRequestHeaders.Add("Authorization", $"Bearer {response.token}");
        }
        catch (ArgumentException e)
        {
            _logger.LogWarning("{Message}", e.Message);
        }

        _tokenExpirationDate = DateTimeOffset.Now.AddMinutes(30);
    }

    public async Task<byte[]?> DownloadImage(string request)
    {
        _logger.Log(LogLevel.Information, "--- GET IMAGE ---");
        _logger.Log(LogLevel.Information, request);
        await CheckTokenExpiration();
        byte[]? result = null;
        try
        {
            var response = await _clientApp.GetByteArrayAsync(request);
            try
            {
                result = response;
                _logger.Log(LogLevel.Debug, result.ToString());
            }
            catch (HttpRequestException httpException)
            {
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.ToString());
                throw;
            }
        }
        catch (HttpRequestException e)
        {
            switch (e.StatusCode)
            {
                case HttpStatusCode.Unauthorized:
                {
                    _logger.Log(LogLevel.Warning, "--- Robert Unauthorized ---");
                    await Authentication();
                    var response = await _clientApp.GetByteArrayAsync(request);

                    try
                    {
                        result = response;
                    }
                    catch (Exception ex)
                    {
                        _logger.Log(LogLevel.Error, ex.ToString());
                        throw;
                    }

                    break;
                }
                case HttpStatusCode.NotFound:
                    _logger.LogWarning("Image not found");
                    break;
                default:
                    _logger.Log(LogLevel.Error, e.ToString());
                    throw;
            }
        }

        return result;
    }

    public async Task<T?> Get<T>(RestRequest request)
    {
        _logger.Log(LogLevel.Information, "--- GET REQUEST ---");
        // _logger.Log(LogLevel.Information, "{S}", _clientApi.BuildUri(request).ToString());
        await CheckTokenExpiration();
        dynamic? result;
        try
        {
            result = await _clientApi.GetAsync<dynamic?>(request);
        }
        catch (HttpRequestException e)
        {
            if (e.StatusCode == HttpStatusCode.Unauthorized)
            {
                await Authentication();
                result = await _clientApi.GetAsync<dynamic?>(request);
            }
            else
            {
                _logger.Log(LogLevel.Error, e.InnerException, "{EMessage}", e.Message);
                throw;
            }
        }

        if (result is not JContainer arrayResult) throw new Exception("Unable to convert get result");
        _logger.Log(LogLevel.Debug, "{S}", arrayResult.ToString());
        return JsonConvert.DeserializeObject<T>(arrayResult.ToString());
    }

    public async Task<T?> Post<T>(RestRequest request)
    {
        _logger.Log(LogLevel.Information, "--- POST REQUEST ---");
        // _logger.Log(LogLevel.Information, "{BuildUri}", _clientApi.BuildUri(request));
        await CheckTokenExpiration();
        foreach (var parameter in request.Parameters)
            _logger.Log(LogLevel.Debug, "{ParameterName} : {SerializeObject}", parameter.Name,
                JsonConvert.SerializeObject(parameter.Value));
        T? result;
        try
        {
            result = await _clientApi.PostAsync<T?>(request);
        }
        catch (HttpRequestException e)
        {
            if (e.StatusCode == HttpStatusCode.Unauthorized)
            {
                _logger.Log(LogLevel.Warning, "--- Robert Unauthorized ---");
                await Authentication();
                result = _clientApi.Post<T?>(request);
            }
            else
            {
                _logger.Log(LogLevel.Error, e.InnerException, "{EMessage}", e.Message);
                throw;
            }
        }

        return result;
    }

    public async Task<T?> Put<T>(RestRequest request)
    {
        _logger.Log(LogLevel.Information, "--- PUT REQUEST ---");
        // _logger.Log(LogLevel.Information, "{BuildUri}", _clientApi.BuildUri(request));
        await CheckTokenExpiration();
        foreach (var parameter in request.Parameters)
            _logger.Log(LogLevel.Debug, "{ParameterName}: {SerializeObject}", parameter.Name,
                JsonConvert.SerializeObject(parameter.Value));

        T? result;
        try
        {
            result = await _clientApi.PutAsync<T?>(request);
        }
        catch (HttpRequestException e)
        {
            if (e.StatusCode == HttpStatusCode.Unauthorized)
            {
                _logger.Log(LogLevel.Warning, "--- Robert Unauthorized ---");
                await Authentication();
                result = _clientApi.Put<T?>(request);
            }
            else
            {
                _logger.Log(LogLevel.Error, e.InnerException, "{EMessage}", e.Message);
                throw;
            }
        }

        return result;
    }
}