using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Robert2.Managers;

namespace Robert2.Extensions;

public static class AddRobert2Service
{
    public static IServiceCollection AddRobert2(this IServiceCollection services, IConfiguration config)
    {
        var robert2Config = new Robert2Config();
        config.GetSection("Robert2Config").Bind(robert2Config);

        services.AddSingleton(robert2Config);

        // managers
        services.AddTransient<MaterialManager>();
        services.AddTransient<CategoryManager>();
        services.AddTransient<BeneficiarieManager>();
        services.AddTransient<CompanyManager>();

        return services;
    }
}