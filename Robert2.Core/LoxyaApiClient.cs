using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;
using Robert2.Helpers;

namespace Robert2;

/// <inheritdoc />
public class LoxyaApiClient : RestClient
{
    /// <inheritdoc />
    public LoxyaApiClient(
        Robert2Config conf,
        string customBaseUrl = ""
    )
        : base(
            new RestClientOptions(string.IsNullOrEmpty(customBaseUrl) ? conf.ApiUrl ?? "" : customBaseUrl)
            {
                Authenticator = new LoxyaApiAuthenticator(conf.ApiUrl!, conf.Identifier!, conf.Password!)
            },
            configureSerialization: config => config.UseNewtonsoftJson()
        )
    {
    }

    /// <inheritdoc />
    public LoxyaApiClient(
        Robert2Config conf,
        HttpMessageHandler handler,
        bool disposeHandler = true
    ) : base(
        handler,
        disposeHandler,
        options =>
        {
            options.Authenticator = new LoxyaApiAuthenticator(conf.ApiUrl!, conf.Identifier!, conf.Password!);
        },
        config => config.UseNewtonsoftJson()
    )
    {
    }
}