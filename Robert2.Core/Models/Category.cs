using Newtonsoft.Json;

namespace Robert2.Models;

public class Category
{
    [JsonProperty("id")]public int Id { get; set; }
    [JsonProperty("name")]public string Name { get; set; }

    [JsonProperty("sub_categories")] public IList<SubCategory> SubCategories { get; set; }
}