using Newtonsoft.Json;

namespace Robert2.Models;

public class Material
{
    [JsonProperty("id")]public int Id { get; set; }
    [JsonProperty("name")]public string Name { get; set; } = "";
    [JsonProperty("description")]public string? Description { get; set; }
    [JsonProperty("reference")]public string Reference { get; set; } = "";

    [JsonProperty("is_unitary")] public bool IsUnitary { get; set; }

    [JsonProperty("park_id")] public int ParkId { get; set; } = 1;

    [JsonProperty("category_id")] public int CategoryId { get; set; } = 1;

    [JsonProperty("sub_category_id")] public int? SubCategoryId { get; set; }

    [JsonProperty("rental_price")] public int RentalPrice { get; set; }

    [JsonProperty("stock_quantity")] public int StockQuantity { get; set; }

    [JsonProperty("out_of_order_quantity")]
    public int? OutOfOrderQuantity { get; set; }

    [JsonProperty("replacement_price")] public int? ReplacementPrice { get; set; }

    [JsonProperty("is_hidden_on_bill")] public bool IsHiddenOnBill { get; set; }

    [JsonProperty("is_discountable")] public bool IsDiscountable { get; set; }

    [JsonProperty("picture")]public string? Picture { get; set; }

    [JsonProperty("note")]public string? Note { get; set; }

    [JsonProperty("available_quantity")] public int AvailableQuantity { get; set; }
    [JsonProperty("tags")]public IEnumerable<Tag> Tags { get; set; } = new List<Tag>();
    [JsonProperty("attributes")]public IEnumerable<Attribute> Attributes { get; set; } = new List<Attribute>();
}