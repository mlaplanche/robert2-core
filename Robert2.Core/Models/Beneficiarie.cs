using Newtonsoft.Json;

namespace Robert2.Models;

public class Beneficiarie : AddressRobert2
{
    [JsonProperty("id")]public int? Id { get; set; }

    [JsonProperty("first_name")] public string FirstName { get; set; } = "";

    [JsonProperty("last_name")] public string LastName { get; set; } = "";
    [JsonProperty("can_make_reservation")] public bool CanMakeReservation { get; set; }

    [JsonProperty("phone")]public string Phone { get; set; } = "";

    // public Company Company { get; set; }
    [JsonProperty("email")]public string Email { get; set; } = "";

    [JsonProperty("reference")]public string? Reference { get; set; }
    [JsonProperty("note")]public string? Note { get; set; }

    [JsonProperty("company_id")] public int? CompanyId { get; set; }
    [JsonProperty("user_id")] public int? UserId { get; set; }

    [JsonProperty("tags")]public IList<string> Tags { get; set; } = new List<string> { "Atomatically generated", "Bénéficiaire" };
}