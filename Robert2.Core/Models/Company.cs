using Newtonsoft.Json;

namespace Robert2.Models;

public class Company : AddressRobert2
{
    [JsonProperty("id")]public int? Id { get; set; }

    [JsonProperty("legal_name")] public string Name { get; set; }

    [JsonProperty("note")]public string? Note { get; set; }
}