using Newtonsoft.Json;

namespace Robert2.Models;

internal class TokenResponse
{
    [JsonProperty("id")]public int Id { get; set; }
    [JsonProperty("pseudo")]public string Pseudo { get; set; } = string.Empty;
    [JsonProperty("email")]public string Email { get; set; } = string.Empty;
    [JsonProperty("group")]public string Group { get; set; } = string.Empty;
    [JsonProperty("language")]public string Language { get; set; } = string.Empty;
    [JsonProperty("first_name")] public string FirstName { get; set; } = string.Empty;
    [JsonProperty("last_name")] public string LastName { get; set; } = string.Empty;
    [JsonProperty("full_name")] public string FullName { get; set; } = string.Empty;
    [JsonProperty("token")]public string Token { get; set; } = string.Empty;
}