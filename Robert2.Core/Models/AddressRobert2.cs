using Newtonsoft.Json;

namespace Robert2.Models;

public class AddressRobert2
{
    [JsonProperty("street")]public string Street { get; set; } = "";

    [JsonProperty("postal_code")] public string PostalCode { get; set; } = "";

    [JsonProperty("locality")]public string Locality { get; set; } = "";
}