using Newtonsoft.Json;

namespace Robert2.Models;

public class SubCategory
{
    [JsonProperty("id")]public int Id { get; set; }
    [JsonProperty("name")]public string Name { get; set; }
    
    [JsonProperty("category_id")]public string CategoryId { get; set; }
}