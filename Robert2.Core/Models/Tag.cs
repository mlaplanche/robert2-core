using Newtonsoft.Json;

namespace Robert2.Models;

public class Tag
{
    [JsonProperty("id")]public int Id { get; set; }
    [JsonProperty("name")]public string Name { get; set; }
}