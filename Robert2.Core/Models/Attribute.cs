using Newtonsoft.Json;

namespace Robert2.Models;
#nullable disable
public class Attribute
{
    [JsonProperty("id")]public int Id { get; set; }
    [JsonProperty("name")]public string Name { get; set; }
    [JsonProperty("type")]public string Type { get; set; }
    [JsonProperty("unit")]public string Unit { get; set; }
    [JsonProperty("value")]public string Value { get; set; }
    [JsonProperty("isTotalisable")]public bool IsTotalisable { get; set; }
}