## [2.0.4](https://gitlab.com/mlaplanche/robert2-core/compare/v2.0.3...v2.0.4) (2023-09-20)


### Bug Fixes

* add subcategory in category ([32c7805](https://gitlab.com/mlaplanche/robert2-core/commit/32c7805ca0f06fc2bdf9768a5a56d1cc072dce37))

## [2.0.3](https://gitlab.com/mlaplanche/robert2-core/compare/v2.0.2...v2.0.3) (2023-08-08)


### Bug Fixes

* **models:** add json properties for all attributes of objects ([501e596](https://gitlab.com/mlaplanche/robert2-core/commit/501e59692d7a052ec2e9fba4435796d231866c35))

## [2.0.2](https://gitlab.com/mlaplanche/robert2-core/compare/v2.0.1...v2.0.2) (2023-08-08)


### Bug Fixes

* **logs:** material download picture url ([76ee723](https://gitlab.com/mlaplanche/robert2-core/commit/76ee723828b17fe0ec3f32126e88a8e469e1b717))
* **logs:** remove slash ([39c75d2](https://gitlab.com/mlaplanche/robert2-core/commit/39c75d2b61e5a43636ce15eb3580b0a20276bc26))
* try new cicd ([8954e60](https://gitlab.com/mlaplanche/robert2-core/commit/8954e60be63de370dea167969b09d537d6b933e9))

## [2.0.1](https://gitlab.com/mlaplanche/robert2-core/compare/v2.0.0...v2.0.1) (2023-08-08)


### Bug Fixes

* **logging:** add logger information for external request ([4162c15](https://gitlab.com/mlaplanche/robert2-core/commit/4162c159333bb50f420c81461fd2ab37f7e3ce7e))
* test features pipeline ([764e6f5](https://gitlab.com/mlaplanche/robert2-core/commit/764e6f5672f86bc5bd77be8a4c3d8b74ad0a3260))

# [2.0.0](https://gitlab.com/mlaplanche/robert2-core/compare/v1.7.0...v2.0.0) (2023-07-28)


### Features

* **api:** update restharp to 110.x ([9b38cfe](https://gitlab.com/mlaplanche/robert2-core/commit/9b38cfeb40d926a87e5370be11e65c34701d68d0))


### BREAKING CHANGES

* **api:** Api communication with Authenticator

# [2.0.0](https://gitlab.com/mlaplanche/robert2-core/compare/v1.7.0...v2.0.0) (2023-07-28)


### Features

* **api:** update restharp to 110.x ([9b38cfe](https://gitlab.com/mlaplanche/robert2-core/commit/9b38cfeb40d926a87e5370be11e65c34701d68d0))


### BREAKING CHANGES

* **api:** Api communication with Authenticator

# [1.7.0](https://gitlab.com/mlaplanche/robert2-core/compare/v1.6.10...v1.7.0) (2023-06-06)


### Features

* update beneficiaries endpoint ([778a503](https://gitlab.com/mlaplanche/robert2-core/commit/778a50353f8d18022a426d87b91d395f8491f3be))

## [1.6.10](https://gitlab.com/mlaplanche/robert2-core/compare/v1.6.9...v1.6.10) (2023-05-03)


### Bug Fixes

* optimise picture errors ([dbe97eb](https://gitlab.com/mlaplanche/robert2-core/commit/dbe97eb89b62128326bb5463e7d4f6e7b55651c2))

## [1.6.9](https://gitlab.com/mlaplanche/robert2-core/compare/v1.6.8...v1.6.9) (2023-05-02)


### Bug Fixes

* image  404 exception ([783b5fe](https://gitlab.com/mlaplanche/robert2-core/commit/783b5fedae36168a792c5ba490ecbac9778e0bbe))

## [1.6.8](https://gitlab.com/mlaplanche/robert2-core/compare/v1.6.7...v1.6.8) (2023-05-02)


### Bug Fixes

* try to fix argument exception on authentification ([e82163d](https://gitlab.com/mlaplanche/robert2-core/commit/e82163dd9335d2e892b9d63a32ebd9e5fe687ed2))

## [1.6.7](https://gitlab.com/mlaplanche/robert2-core/compare/v1.6.6...v1.6.7) (2023-04-28)


### Bug Fixes

* client authentification ([042e8d8](https://gitlab.com/mlaplanche/robert2-core/commit/042e8d84ea0027c6d8138f746f5f2d065f458784))

## [1.6.6](https://gitlab.com/mlaplanche/robert2-core/compare/v1.6.5...v1.6.6) (2023-04-26)


### Bug Fixes

* company manager ([a824b6a](https://gitlab.com/mlaplanche/robert2-core/commit/a824b6a1cca77247a4479da453db1e20dbc8059c))

## [1.6.5](https://gitlab.com/mlaplanche/robert2-core/compare/v1.6.4...v1.6.5) (2023-04-26)


### Bug Fixes

* update company comportement ([c6ed82f](https://gitlab.com/mlaplanche/robert2-core/commit/c6ed82f76de1a78b29c8934e83cc497e638f726e))

## [1.6.4](https://gitlab.com/mlaplanche/robert2-core/compare/v1.6.3...v1.6.4) (2023-04-14)


### Bug Fixes

* **await:** Add async/await method ([eece8da](https://gitlab.com/mlaplanche/robert2-core/commit/eece8da78d4542a4280667efe748347c688d85db))

## [1.6.3](https://gitlab.com/mlaplanche/robert2-core/compare/v1.6.2...v1.6.3) (2023-03-29)


### Bug Fixes

* **beneficiare:** fix return value by robert ([81c4c80](https://gitlab.com/mlaplanche/robert2-core/commit/81c4c80d6e822886128d320ef99bdec6bbc7be67))

## [1.6.2](https://gitlab.com/mlaplanche/robert2-core/compare/v1.6.1...v1.6.2) (2023-03-29)


### Bug Fixes

* **beneficiare:** change person to beneficiare ([fccb23a](https://gitlab.com/mlaplanche/robert2-core/commit/fccb23aa278c0a23f6c5808e975c5331bec1bfce))

## [1.6.1](https://gitlab.com/mlaplanche/robert2-core/compare/v1.6.0...v1.6.1) (2023-03-29)


### Bug Fixes

* **MaterialModel:** Change RemainingQuantity to AvailableQuantity ([3bb3684](https://gitlab.com/mlaplanche/robert2-core/commit/3bb3684198cc188d5a646c1bb75402495f068bf5))

# [1.6.0](https://gitlab.com/mlaplanche/robert2-core/compare/v1.5.0...v1.6.0) (2022-12-19)


### Bug Fixes

* **entity:** add public URL in conf ([5885761](https://gitlab.com/mlaplanche/robert2-core/commit/5885761e66df6bd9ebad0e2a9ed60ef4e55a592b))
* **photo:** no throw exception when photo is 404 ([2f1aa0d](https://gitlab.com/mlaplanche/robert2-core/commit/2f1aa0dfefdad118053eed51b188f888fb3fcb9a))


### Features

* **core:** add Get method for company ([f2dc139](https://gitlab.com/mlaplanche/robert2-core/commit/f2dc139b21e21ccec0a23012fd78231d2ff1cafd))

# [1.5.0](https://gitlab.com/mlaplanche/robert2-core/compare/v1.4.0...v1.5.0) (2022-11-03)


### Features

* **core:** Add get person by email ([80e41b3](https://gitlab.com/mlaplanche/robert2-core/commit/80e41b3c9458aa31d1700ca914b3818064fe8a90))

# [1.4.0](https://gitlab.com/mlaplanche/robert2-core/compare/v1.3.2...v1.4.0) (2022-11-03)


### Bug Fixes

* **core:** fix dependency injection of robert client ([22cedde](https://gitlab.com/mlaplanche/robert2-core/commit/22ceddeed07faee98d3b1879f54caba30314d5aa))
* **log:** Add logging in client ([a876964](https://gitlab.com/mlaplanche/robert2-core/commit/a8769645a647b3d3ada0eb34d01bc8532155d1f2))


### Features

* **core:** refactoring resharper ([3e99b12](https://gitlab.com/mlaplanche/robert2-core/commit/3e99b1251b75a9990da87d14dda5c5167c30c340))

## [1.3.3](https://gitlab.com/mlaplanche/robert2-core/compare/v1.3.2...v1.3.3) (2022-11-03)


### Bug Fixes

* **core:** fix dependency injection of robert client ([22cedde](https://gitlab.com/mlaplanche/robert2-core/commit/22ceddeed07faee98d3b1879f54caba30314d5aa))
* **log:** Add logging in client ([a876964](https://gitlab.com/mlaplanche/robert2-core/commit/a8769645a647b3d3ada0eb34d01bc8532155d1f2))

## [1.3.2](https://gitlab.com/mlaplanche/robert2-core/compare/v1.3.1...v1.3.2) (2022-11-02)


### Bug Fixes

* **core:** update call for image ([c50dc3f](https://gitlab.com/mlaplanche/robert2-core/commit/c50dc3fdee8acb7539eb7450c584b61ae8a039d2))

## [1.3.1](https://gitlab.com/mlaplanche/robert2-core/compare/v1.3.0...v1.3.1) (2022-11-02)


### Bug Fixes

* **core:** resharper refactoring ([6c92255](https://gitlab.com/mlaplanche/robert2-core/commit/6c92255a1d98884408112afbefa3a19ca5776916))

# [1.3.0](https://gitlab.com/mlaplanche/robert2-core/compare/v1.2.0...v1.3.0) (2022-09-25)


### Features

* **core:** add attributes in Material ([014b6f2](https://gitlab.com/mlaplanche/robert2-core/commit/014b6f270371ff743c222b516c3f3c3d43f86851))

# [1.2.0](https://gitlab.com/mlaplanche/robert2-core/compare/v1.1.0...v1.2.0) (2022-08-26)


### Features

* **common:** add tags on material object ([86a94f3](https://gitlab.com/mlaplanche/robert2-core/commit/86a94f38b9916a88c72082ad74aa33e562dd7f3f))

# [1.1.0](https://gitlab.com/mlaplanche/robert2-core/compare/v1.0.2...v1.1.0) (2022-08-23)


### Features

* **elements:** Add method for download materials images ([ba86ae4](https://gitlab.com/mlaplanche/robert2-core/commit/ba86ae469d5c71e66ef58f355f5dee36bb366bf9))
* **http:** Add HttpClient in Robert2Client object for material images ([5ac3033](https://gitlab.com/mlaplanche/robert2-core/commit/5ac30336215b68744992a95bf0ac11ba386e1461))

## [1.0.2](https://gitlab.com/mlaplanche/robert2-core/compare/v1.0.1...v1.0.2) (2022-07-19)


### Bug Fixes

* **core:** catch error on 401exception ([b20ff7d](https://gitlab.com/mlaplanche/robert2-core/commit/b20ff7df09db5f9604e55ebb95a3b7490520100d))

## [1.0.1](https://gitlab.com/mlaplanche/robert2-core/compare/v1.0.0...v1.0.1) (2022-07-08)


### Bug Fixes

* **ci/cd:**  @semantic-release/git ([2120fbb](https://gitlab.com/mlaplanche/robert2-core/commit/2120fbb8c3c93d8f39e6e62e43127e32e458c58e))
* **ci/cd:** fix ci/cd for semantic release ([62069a3](https://gitlab.com/mlaplanche/robert2-core/commit/62069a374c78142fe4d96c78960af3cec9cf694b))
* **ci/cd:** update .releaserc.json ([1624949](https://gitlab.com/mlaplanche/robert2-core/commit/1624949d4d0503f616e36594d06de1ba94d83c49))
* **cicd:** remove update file ([a64cca4](https://gitlab.com/mlaplanche/robert2-core/commit/a64cca401dc72cae1bffae2b3d623da8ca4bc420))
* **cicd:** update .releaserc.json ([f3a869f](https://gitlab.com/mlaplanche/robert2-core/commit/f3a869fdb9f48f6107d7160ed746518baaaf71af))
* **cicd:** update gitlab-ci.yml ([0673168](https://gitlab.com/mlaplanche/robert2-core/commit/0673168a7ffd526dac87452724c28cef50baf76b))
* **js:** add package.json ([70917a9](https://gitlab.com/mlaplanche/robert2-core/commit/70917a92d5d81997ebbf39bc68f9cc0e1b207a66))
